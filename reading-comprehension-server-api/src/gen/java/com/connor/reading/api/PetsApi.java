package com.connor.reading.api;

import com.connor.reading.dto.Error;
import com.connor.reading.dto.Pet;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import io.swagger.annotations.*;

import java.io.InputStream;
import java.util.Map;
import java.util.List;
import javax.validation.constraints.*;
import javax.validation.Valid;

@Path("/pets")
@Api(description = "the pets API")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJAXRSSpecServerCodegen", date = "2020-08-10T18:12:16.465034200+03:00[Europe/Helsinki]")
public interface PetsApi {

    @POST
    @Produces({ "application/json" })
    @ApiOperation(value = "Create a pet", notes = "", tags={ "pets",  })
    @ApiResponses(value = { 
        @ApiResponse(code = 201, message = "Null response", response = Void.class),
        @ApiResponse(code = 200, message = "unexpected error", response = Error.class) })
    Response createPets();

    @GET
    @Produces({ "application/json" })
    @ApiOperation(value = "List all pets", notes = "", tags={ "pets",  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "A paged array of pets", response = Pet.class, responseContainer = "List"),
        @ApiResponse(code = 200, message = "unexpected error", response = Error.class, responseContainer = "List") })
    Response listPets(@QueryParam("limit")   @ApiParam("How many items to return at one time (max 100)")  Integer limit);

    @GET
    @Path("/{petId}")
    @Produces({ "application/json" })
    @ApiOperation(value = "Info for a specific pet", notes = "", tags={ "pets" })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Expected response to a valid request", response = Pet.class, responseContainer = "List"),
        @ApiResponse(code = 200, message = "unexpected error", response = Error.class, responseContainer = "List") })
    Response showPetById(@PathParam("petId") @ApiParam("The id of the pet to retrieve") String petId);
}
