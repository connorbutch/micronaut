package com.connor.reading;

import javax.ws.rs.core.Response;

public class DummyApiImpl implements DummyApi {
    @Override
    public Response createPets() {
        return Response.ok().build();
    }

    @Override
    public Response listPets(Integer limit) {
        return Response.ok().build();
    }

    @Override
    public Response showPetById(String petId) {
        return Response.ok().build();
    }
}
