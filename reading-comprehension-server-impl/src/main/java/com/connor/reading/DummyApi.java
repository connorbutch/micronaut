package com.connor.reading;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/dummy")
public interface DummyApi {


        @POST
        @Produces({ "application/json" })

        Response createPets();

        @GET
        @Produces({ "application/json" })

        Response listPets(@QueryParam("limit")   Integer limit);

        @GET
        @Path("/{petId}")
        @Produces({ "application/json" })

        Response showPetById(@PathParam("petId") String petId);
}
